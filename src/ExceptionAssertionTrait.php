<?php

namespace NecLimDul\PhpUnitExceptions;

use Closure;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Constraint\LogicalAnd;
use PHPUnit\Framework\Constraint\StringContains;
use Throwable;

trait ExceptionAssertionTrait
{
  /**
   * Asserts the given callback throws an exception when called.
   *
   * @param \Closure $callback
   *   Callback that executes the test code that will throw an exception.
   * @param class-string<\Throwable> $class
   *   Expected exception class.
   * @param string|null $message
   *   Expected exception message.
   * @param int|string|null $code
   *   Expected exception code.
   */
    protected function assertThrows(
        Closure $callback,
        string $class = Throwable::class,
        ?string $message = null,
        int|string $code = null
    ): void {
        try {
            $callback();
            Assert::fail(
                sprintf('Failed asserting that exception of type "%s" was thrown.', $class)
            );
        } catch (Throwable $exception) {
            $this->assertException($class, $exception, $message, $code);
        }
    }

    /**
     * Assert that the correct exception was thrown.
     *
     * @param class-string<\Throwable> $class
     *   Expected exception class.
     * @param \Throwable $exception
     *   Actual exception to compare.
     * @param string|null $message
     *    Expected exception message.
     * @param int|string|null $code
     *    Expected exception code.
     */
    public function assertException(
        string $class,
        Throwable $exception,
        string $message = null,
        int|string $code = null
    ): void {
        Assert::assertInstanceOf(
            $class,
            $exception,
            sprintf('Failed asserting that exception of type "%s" was thrown.', $class)
        );

        if (isset($message)) {
            Assert::assertThat(
                $exception->getMessage(),
                LogicalAnd::fromConstraints(
                    new StringContains($message),
                    static::logicalNot(static::isNull())
                ),
                sprintf(
                    'Failed asserting that exception with message "%s" is thrown',
                    $message
                )
            );
        }
        if (isset($code)) {
            Assert::assertEquals(
                $code,
                $exception->getCode(),
                sprintf(
                    'Failed asserting that exception with code "%s" is thrown',
                    $code
                )
            );
        }
    }
}
