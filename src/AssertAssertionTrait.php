<?php

namespace NecLimDul\PhpUnitExceptions;

use AssertionError;
use Closure;

trait AssertAssertionTrait
{
    use ExceptionAssertionTrait;

    public function assertAssertion(Closure $callback, ?string $message = null): void
    {
        // Note, this is a fallback to avoid unhelpful assertion failures. Test should generally have their own
        // requirement testing.
        if (ini_get('zend.assertions') <= 0) {
            $this->markTestSkipped('Assertions are not enabled. Set zend.assertions to 1 to run this test.');
        } else {
            $this->assertThrows($callback, AssertionError::class, $message);
        }
    }
}
