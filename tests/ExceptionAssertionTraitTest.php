<?php

namespace NecLimDul\PhpUnitExceptions\Tests;

use Exception;
use InvalidArgumentException;
use NecLimDul\PhpUnitExceptions\ExceptionAssertionTrait;
use PHPUnit\Framework\ExpectationFailedException;
use PHPUnit\Framework\TestCase;
use UnderflowException;

/**
 * @coversDefaultClass \NecLimDul\PhpUnitExceptions\ExceptionAssertionTrait
 */
class ExceptionAssertionTraitTest extends TestCase
{
    use ExceptionAssertionTrait;

    /**
     * @return array<array{null|string, null|string|int}>
     */
    public static function provideSuccess(): array
    {
        return [
            [null, null],
            ['', null],
            [
                'Test Message',
                123,
            ],
            [
                'Test Message',
                123,
            ],
        ];
    }

    /**
     * @dataProvider provideSuccess
     */
    public function testExceptionSuccess(?string $message, ?int $code): void
    {
        $called = false;
        $this->assertThrows(function () use (&$called, $message, $code) {
            $called = true;
            if (isset($message)) {
                if (isset($code)) {
                    throw new InvalidArgumentException($message, $code);
                }
                throw new InvalidArgumentException($message);
            } else {
                throw new InvalidArgumentException();
            }
        }, InvalidArgumentException::class, $message, $code);
        $this->assertTrue($called, 'Callback called.');
    }

    public function testFailures(): void
    {
        try {
            $this->assertThrows(fn() => '', InvalidArgumentException::class);
            $this->fail('Missmatch exception not detected');
        } catch (Exception $e) {
            $this->assertException(
                ExpectationFailedException::class,
                $e,
                "Failed asserting that exception of type \"InvalidArgumentException\" was thrown.\n" .
                'Failed asserting that an object is an instance of class InvalidArgumentException.',
            );
        }
        try {
            $this->assertThrows(function () {
                throw new InvalidArgumentException();
            }, UnderflowException::class);
            $this->fail('Missmatch exception not detected');
        } catch (Exception $e) {
            $this->assertException(
                ExpectationFailedException::class,
                $e,
                "Failed asserting that exception of type \"UnderflowException\" was thrown.\n" .
                'Failed asserting that an object is an instance of class UnderflowException.',
            );
        }
        try {
            $this->assertThrows(function () {
                throw new InvalidArgumentException('Test Message');
            }, InvalidArgumentException::class, 'Bad Message');
            $this->fail('Bad exception message not detected');
        } catch (Exception $e) {
            $this->assertException(
                ExpectationFailedException::class,
                $e,
                'Failed asserting that exception with message "Bad Message" is thrown',
            );
        }
        try {
            $this->assertThrows(function () {
                throw new InvalidArgumentException();
            }, InvalidArgumentException::class, 'Bad Message');
            $this->fail('Missing exception message not detected');
        } catch (Exception $e) {
            $this->assertException(
                ExpectationFailedException::class,
                $e,
                'Failed asserting that exception with message "Bad Message" is thrown',
            );
        }
        try {
            $this->assertThrows(function () {
                throw new InvalidArgumentException('Test Message', 123);
            }, InvalidArgumentException::class, 'Test Message', 345);
            $this->fail('Bad exception code not detected');
        } catch (Exception $e) {
            $this->assertException(
                ExpectationFailedException::class,
                $e,
                'Failed asserting that exception with code "345" is thrown',
            );
        }
    }
}
