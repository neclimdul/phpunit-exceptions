<?php

namespace NecLimDul\PhpUnitExceptions\Tests;

use NecLimDul\PhpUnitExceptions\AssertAssertionTrait;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \NecLimDul\PhpUnitExceptions\AssertAssertionTrait
 */
class AssertAssertionTraitTest extends TestCase
{
    use AssertAssertionTrait;

    /**
     * @covers ::assertAssertion
     * @requires setting zend.assertions 1
     */
    public function testAssertAssertion(): void
    {
        $this->assertAssertion(function () {
          /** @phpstan-ignore-next-line */
            assert(false, 'Test message');
        }, 'Test message');
    }
}
