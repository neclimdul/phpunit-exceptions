## PHPUnit Exceptions

This projects aims to provide some simple tools to make testing exceptions easier in the PHPUnit testing framework. 

It was inspired by similar functionality in [Laravel](https://github.com/laravel/framework/blob/2d002849a16ad131110a50cbea4d64dbb78515a3/src/Illuminate/Foundation/Testing/Concerns/InteractsWithExceptionHandling.php#L161).

### Installation

```shell
composer require neclimdul/phpunit_exceptions
```

### Usage


```php
<?php

use NecLimDul\PhpUnitExceptions\ExceptionAssertionTrait;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request
use Symfony\Component\HttpKernel\Exception\HttpException;

class MyTest extends TestCase {

  use ExceptionAssertionTrait;

  public function testBadRequest() {
    $request = new Request();
    $this->assertThrows(function () use ($request) {
      (new MyMiddleware())->handle($request, function ($request) {});
    }, HttpException::class);
  }

}

```

